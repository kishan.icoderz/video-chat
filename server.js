const express = require("express");
const app = express();
const http = require('http');
const server = http.createServer(app);
const { Server } = require("socket.io");
const { v4: uuidv4 } = require("uuid");
const io = new Server(server);


const { ExpressPeerServer } = require("peer");
app.set("view engine", "ejs");
const peerServer = ExpressPeerServer(server, {
  debug: true,
});
app.use("/peerjs", peerServer);
app.use(express.static("public"));
app.get("/", (req, res) => {
  res.redirect(`/${uuidv4()}`);
});
app.get("/:room", (req, res) => {
res.render("room", { roomId: req.param.room });
});


io.on("connection", (socket) => {  
  socket.on("join-room", (roomId, userId, userName) => {
    console.log("roomId",roomId);
    socket.join(roomId);
    // setTimeout(()=>{
    //   socket.to(roomId).broadcast.emit("user-connected", userId);
    // }, 1000);
    setTimeout(()=>{
      socket.broadcast.to(roomId).broadcast.emit("user-connected", userId);
    }, 1000)
    socket.on("message", (message) => {
      io.to(roomId).emit("createMessage", message, userName);
    });
  });
});
server.listen(3030);